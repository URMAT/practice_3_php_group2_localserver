<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\User;

use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_facebook = "facebook";
    const SOC_NETWORK_google = "google";

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     */
    public function createNewAbstractUser($user, array $data, bool $encodePassword = true) {
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->setVkId($data['vkId']??null);
        $user->setFaceBookId($data['faceBookId']??null);
        $user->setGoogleId($data['googleId']??null);
        $user->setRoles($data['roles'] ?? ['ROLE_USER']);

        if($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     */
    public function createNewUser(array $data, bool $encodePassword = true) {
        $user = new User();
        $user = $this->createNewAbstractUser($user, $data, $encodePassword);

        return $user;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     */
    public function createNewLandlord(array $data) {
        $landlord = new Landlord();
        $object = $this->createNewAbstractUser($landlord, $data);
        return $object;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     */
    public function createNewTenant(array $data) {
        $tenant = new Tenant();
        $object = $this->createNewAbstractUser($tenant, $data);
        return $object;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return  md5($password) . md5($password . '2');
    }

    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}
