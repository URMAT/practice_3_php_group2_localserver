<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kind_object', ChoiceType::class, [
                'choices' => [
                    'Выберите тип' => null,
                    'Пансионат' => 'pension',
                    'Коттедж' => 'cottage'
                ],
            ])
            ->add('name_object', TextType::class)
            ->add('full_name', TextType::class)
            ->add('quantity_room', TextType::class)
            ->add('price', NumberType::class)
            ->add('contact_phone', TelType::class, [
                'label' => 'Введите номер телефона по форме 0700-11-22-33',
                'attr' => [
                    'pattern' => '0[0-9]{3}-[0-9]{2}-[0-9]{2}-[0-9]{2}'
                ]
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('Next', SubmitType::class)
        ;
    }
}
