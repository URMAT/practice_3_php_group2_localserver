<?php

namespace App\Entity;

class Pension extends BookingObject
{
    private $cook;

    private $swimming_pool;

    private $tv;

    public function __toArray(){
        $pension = [];
        $pension['cook'] = $this->getCook();
        $pension['address'] = $this->getAddress();
        $pension['full_name'] = $this->getFullName();
        $pension['contact_phone'] = $this->getContactPhone();
        $pension['price'] = $this->getPrice();
        $pension['name_object'] = $this->getNameObject();
        $pension['swimming_pool'] = $this->getSwimmingPool();
        $pension['tv'] = $this->getTv();
        $pension['quantity_room'] = $this->getQuantityRoom();
        return $pension;
    }

    public function setCook($cook)
    {
        $this->cook = $cook;
        return $this;
    }

    public function getCook()
    {
        return $this->cook;
    }

    public function setSwimmingPool($swimming_pool)
    {
        $this->swimming_pool = $swimming_pool;
        return $this;
    }

    public function setTv($tv)
    {
        $this->tv = $tv;
        return $this;
    }

    public function getSwimmingPool()
    {
        return $this->swimming_pool;
    }

    public function getTv()
    {
        return $this->tv;
    }

}
