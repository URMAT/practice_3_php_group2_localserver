<?php

namespace App\Entity;

class BookingObject
{

    private $id;

    protected $name_object;

    private $full_name;

    private $quantity_room;

    /**
     * @var float
     */
    private $price;

    private $contact_phone;

    private $address;

    public function __toArray(){

    }

    public function setNameObject($name_object)
    {
        $this->name_object = $name_object;
        return $this;
    }

    public function getNameObject()
    {
        return $this->name_object;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
        return $this;
    }

    public function getFullName()
    {
        return $this->full_name;
    }

    public function setQuantityRoom($quantity_room)
    {
        $this->quantity_room = $quantity_room;
        return $this;
    }

    public function getQuantityRoom()
    {
        return $this->quantity_room;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setContactPhone($contact_phone)
    {
        $this->contact_phone = $contact_phone;
        return $this;
    }

    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }


}
